import 'dart:convert';
import 'dart:typed_data';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'EncryptDecrypt',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController encryptController = TextEditingController();
  TextEditingController decryptController = TextEditingController();
  var encryptedString;
  var encrypter;
  var iv;
  @override
  void initState() {
    super.initState();

    const ivString = "gqLOHUioQ0QjhuvI";
    List<int> ivInt = utf8.encode(ivString);
    Uint8List ivBytes = Uint8List.fromList(ivInt);

    // bbC2H19lkVbQDfak
    const keyString = "bbC2H19lkVbQDfakxcrtNMQdd0FloLyw";
    List<int> keyInt = utf8.encode(keyString);
    Uint8List keyBytes = Uint8List.fromList(keyInt);
    final key = encrypt.Key(keyBytes);
    iv = encrypt.IV(ivBytes);
    encrypter = encrypt.Encrypter(
        encrypt.AES(key, mode: encrypt.AESMode.cbc, padding: "PKCS7"));
  }

  @override
  Widget build(BuildContext context) {
    final key = new GlobalKey<ScaffoldState>();
    return Scaffold(
      key: key,
      appBar: AppBar(
        title: Text("Encrypt Decrypt "),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: TextField(
                controller: encryptController,
                decoration: InputDecoration(
                  hintText: 'Enter String',
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.red),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                      borderSide: BorderSide(color: Colors.blue[400])),
                  isDense: true,
                  // Added this
                  contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 10),
                ),
                cursorColor: Colors.white,
              ),
            ),
            SizedBox(height: 20),
            RaisedButton(
              onPressed: () async {
                final encrypted = encrypter.encrypt(encryptController.text, iv: iv);
                setState(() {
                  encryptedString = encrypted.base64;

                });
              },
              child: Text("Encrypt"),
            ),
            SizedBox(height: 40),
            Padding(
              padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
              child: TextField(
                controller: decryptController,
                decoration: InputDecoration(
                  hintText: 'Enter String',
                  hintStyle: TextStyle(color: Colors.black),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(10.0),
                    borderSide: BorderSide(color: Colors.red),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderRadius: new BorderRadius.circular(10.0),
                      borderSide: BorderSide(color: Colors.blue[400])),
                  isDense: true,
                  // Added this
                  contentPadding: EdgeInsets.fromLTRB(10, 20, 10, 10),
                ),
                cursorColor: Colors.white,
              ),
            ),
            SizedBox(height: 20),

            RaisedButton(
              onPressed: () {
                final decrypted = encrypter.decrypt64(decryptController.text, iv: iv);
                setState(() {
                  encryptedString = decrypted;
                });
              },
              child: Text("Decrypt"),
            ),
            SizedBox(height: 20),

            GestureDetector(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Text(
                  encryptedString != null ? encryptedString : "",
                  style: TextStyle(fontSize: 20, color: Colors.lightBlue),
                ),
              ),
              onTap: () {
                Clipboard.setData(new ClipboardData(text: encryptedString));
                key.currentState.showSnackBar(new SnackBar(
                  content: new Text("Copied to Clipboard"),
                ));
              },
            )
          ],
        ),
      ),
    );
  }


}
